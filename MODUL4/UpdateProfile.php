<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    session_start();
    include 'koneksi.php';
    $id = $_SESSION['id'];
    $query = "SELECT * FROM users_table WHERE id='$id'";
    $result = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_array($result)) {
        $id = $row['id'];
        $email = $row['email'];
        $username = $row['username'];
        $mobile = $row['mobile_number'];
        $pass = $row['password'];
    }
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Update Data Profile</title>
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <div class="container-fluid">
            <div class="col-12">
                <div class="row">
                    <img src="EAD.png " alt="logoead" width="110" height="40">
                    <ul class="nav navbar-nav navbar-right nav-index ml-auto">
                        <?php
                        if (!isset($_SESSION['user'])) {
                            echo "<li><a class='nav-item nav-link' href='#' data-toggle='modal' data-target='#Modal-login'>Login</a></li>
                        <li><a class='nav-item nav-link' href='#' data-toggle='modal' data-target='#Modal-registrasi'>Registrasi</a></li>";
                        } else {
                            echo "<li class='nav-item dropdown' >
                        <a class='nav-link dropdown-toggle' data-toggle='dropdown' id='navbarDropdownMenuLink'
                        href='#' role='button' aria-haspopup='true' aria-expanded='false'>" . $_SESSION['user'] . "</a>
                        <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
                            <a class='dropdown-item' href='Home.php'>Home</a>
                            <a class='dropdown-item' href='Logout.php'>Logout</a>
                            </div>                    
                            </li>              
                            ";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-6">
                <form action="Update.php" method="POST">
                    <div class="form-group">
                        <label for="Email_Address">Email Address</label>
                        <input type="text" class="form-control" name="email" value="<?php echo $email ?>">
                        <br>
                        <label><?php echo $email ?> </label>
                    </div>
                    <div class="form-group">
                        <label for="Username">Username</label>
                        <input type="text" class="form-control" name="username" value="<?php echo $username ?>">
                    </div>
                    <div class="form-group">
                        <label for="Number">Mobile Number</label>
                        <input type="number" class="form-control" name="number" value="<?php echo $mobile ?>">
                    </div>
                    <div class="form-group">
                        <label for="Password">Password</label>
                        <input type="text" class="form-control" name="password" value="<?php echo $pass ?>">
                    </div>
                    <div class="modal-footer">
                        <a href="Home.php" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>

                <div class="card-footer text-muted">
                    <center>
                        © EAD STORE
                </div>
</body>

</html>