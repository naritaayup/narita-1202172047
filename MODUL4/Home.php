<?php

include "koneksi.php";
session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
        .header {
            margin-left: 13%;
            width: 72%;
            height: 200px;
            background-color: #C2E8CE;
            margin-bottom: 20px;
        }

        .header-paragraph {
            padding-top: 50px;
        }

        .tinggi {
            height: 250px;
            align margin-right: auto;
        }
    </style>
    <title>Home Modul4</title>
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <div class="container-fluid">
            <div class="col-12">
                <div class="row">
                    <img src="EAD.png " alt="logoead" width="110" height="40">
                    <ul class="nav navbar-nav ml-auto">
                        <?php
                        if (!isset($_SESSION['user'])) {
                            ?>
                            <li><a class='nav-item nav-link' href='#' data-toggle='modal' data-target='#login'>Login</a></li>
                            <li><a class='nav-item nav-link' href='#' data-toggle='modal' data-target='#registrasi'>Registrasi</a></li>
                        <?php  } else { ?>
                            <li><a class='nav-item nav-link' href='Cart.php'><img src="Cart.png" alt="Logocart"></a></li>
                            <li class='nav-item dropdown'>
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="navbarDropdownMenuLink" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    <?php echo $_SESSION['user']; ?> </a>
                                <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
                                    <a class='dropdown-item' href='UpdateProfile.php'>Setting</a>
                                    <a class='dropdown-item' href='Logout.php'>Logout</a>
                                </div>
                            </li>
                        <?php  }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <br>
    <div class="header">
        <div class="header-paragraph">
            <h2>Hello Coders</h2>
            <p>Welcome to our store, please take a look for the products you might buy</p>
        </div>
    </div>
    <div class="contrainer-fuild">
        <div class="row justify-content-md-center">
            <div class="col-3">
                <div class="card" style="width: 18rem;">
                    <img src="Web.png" class="card-img-top" alt="LogoWeb">
                    <div class="card-body tinggi">
                        <h6 class="card-title">Learning Basic Web Programming</h6>
                        <p class="card-text">Rp.210.000,-</p>
                        <p class="card-text ">
                            Want to be able to make a website? Learn basic components such as HTML, CSS and JavaScript in this class curriculum.</p>
                    </div>
                    <div class="card-footer">
                        <form action="buy.php" method="POST">
                            <input type="hidden" value="Learning Basic Web Programming" name="product">
                            <input type="hidden" value="210000" name="price">
                            <input type="submit" value="Buy" class="btn btn-primary col-sm-12">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card" style="width: 18rem;">
                    <img src="Java.png" class="card-img-top" alt="LogoJava">
                    <div class="card-body tinggi">
                        <h6 class="card-title">Starting Programming in Java</h6>
                        <p class="card-text">Rp.150.000,-</p>
                        <p class="card-text ">
                            Learning Java Language for you who want to learn the most popular Object-Oriented Programming (PBO) concepts for developing applications.</p>
                    </div>
                    <div class="card-footer">
                        <form action="buy.php" method="POST">
                            <input type="hidden" value="Starting Programming in Java" name="product">
                            <input type="hidden" value="150000" name="price">
                            <input type="submit" value="Buy" class="btn btn-primary col-sm-12">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card" style="width: 18rem;">
                    <img src="Py.png" class="card-img-top" alt="LogoPy">
                    <div class="card-body tinggi">
                        <h6 class="card-title">Starting Programming in Python</h6>
                        <p class="card-text">Rp.200.000,-</p>
                        <p class="card-text ">
                            Learning Python -Fundamental various current industry trends: Data Science, Machine Learning, Infrastructure-management.</p>
                    </div>
                    <div class="card-footer">
                        <form action="buy.php" method="POST">
                            <input type="hidden" value="Starting Programming in Python" name="product">
                            <input type="hidden" value="200000" name="price">
                            <input type="submit" value="Buy" class="btn btn-primary col-sm-12">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="Login.php" method="POST">
                        <div class="form-group">
                            <label for="InputEmail1">Email address</label>
                            <input type="email" class="form-control" name="InputEmail1" id="InputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="InputPass1">Password</label>
                            <input type="password" class="form-control" name="InputPass1" id="InputPass1" aria-describedby="emailHelp" placeholder="Password">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" name="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 2 -->
    <div class="modal fade" id="registrasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="Register.php" method="POST">
                        <div class="form-group">
                            <label for="InputEmail2">Email address</label>
                            <input type="email" class="form-control" name="InputEmail2" id="InputEmail2" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="InputUsername">Username</label>
                            <input type="text" class="form-control" name="InputUsername" id="InputUsername" aria-describedby="emailHelp" placeholder="Enter Username">
                        </div>
                        <div class="form-group">
                            <label for="InputPass2">Password</label>
                            <input type="password" class="form-control" name="InputPass2" id="InputPass2" aria-describedby="emailHelp" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="ConfirmPass">Confirm Password</label>
                            <input type="password" class="form-control" name="ConfirmPass" id="ConfirmPass" aria-describedby="emailHelp" placeholder="Confirm Password">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="card-footer text-muted">
        <center>
            © EAD STORE
    </div>
</body>

</html>