<?php

session_start();

include "koneksi.php";

$id = $_SESSION['id'];

$sql = "SELECT * FROM `cart_table` WHERE user_id = $id";

$exec = mysqli_query($conn, $sql);

$i = 0;
$total = 0;
$rows = [];
while ($row = mysqli_fetch_assoc($exec)) {
    $rows[$i]['no'] = ($i + 1);
    $rows[$i]['id'] = $row['id'];
    $rows[$i]['product'] = $row['product'];
    $rows[$i]['price'] = number_format($row['price'], 2);

    $total += $row['price'];

    $i++;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
        .text-black a{
            color : black !important;
        }
    </style>
    <title>Cart Modul4</title>
</head>

<body>
    <nav class="navbar navbar-light bg-light text-black">
        <div class="container-fluid">
                <img src="EAD.png " alt="logoead" width="110" height="40">
                <ul class="nav ml-auto ">
                    <li><a class='nav-item nav-link' href='Cart.php'><img src="Cart.png" alt="Logocart"></a></li>
                    <li class='nav-item dropdown '>
                        <a class="nav-link dropdown-toggle text-black" data-toggle="dropdown" id="navbarDropdownMenuLink" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php echo $_SESSION['user']; ?> </a>
                        <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
                            <a class='dropdown-item' href='UpdateProfile.php'>Setting</a>
                            <a class='dropdown-item' href='Logout.php'>Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>


    </nav>
    <br>
    <div class="table table-borderless contBox">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Product</th>
                    <th scope="col">Price</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if (isset($rows)) {
                    foreach ($rows as $row) { ?>
                        <tr>
                            <td align="center" style="vertical-align: middle;"><?php echo $row['no'] ?></td>
                            <td style="vertical-align: middle;"><?php echo  $row['product'] ?></td>
                            <td align="right" style="vertical-align: middle;"><?php echo $row['price'] ?></td>
                            <td align="center" style="vertical-align: middle;">
                                <form action="delete.php" method="POST">
                                    <input type="hidden" name="id" value="<?= $row['id']; ?>">
                                    <button type="submit" class="btn btn-danger">x</button>
                                </form>
                            </td>
                        </tr>
                <?php }
                } ?>
                <tr>
                    <td colspan="2"><b>Total</b></td>
                    <td align="right"><?= number_format($total, 2); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</body>

</html>