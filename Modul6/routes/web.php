<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@showProfile');
Route::get('/editprofile', 'HomeController@editProfile');
Route::put('/updateProfile', 'HomeController@updateProfile');
Route::get('/addPost', 'PostController@index');
Route::post('/postdata', 'PostController@store');
Route::post('/postComment/{id}', 'KomentarPostsController@store');
Route::get('/detailPost/{id}', 'PostController@show');
Route::get('/postlike/{id}','PostController@likes')->name('postlike');