<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komentar_posts extends Model
{
    protected $fillable = [
        'user_id', 'post_id', 'comment'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function posts(){
        return $this->belongsTo('App\post', 'post_id');
    }
}
