<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $fillable = [
        'user_id', 'caption', 'image', 'likes'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function komentar_posts(){
        return $this->hasMany('App\komentar_posts', 'post_id');
    }
}
