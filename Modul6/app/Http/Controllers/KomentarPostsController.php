<?php

namespace App\Http\Controllers;

use App\komentar_posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KomentarPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $comment = new komentar_posts();
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $id;
        $comment->comment = $request->input('comment');
        $comment->save();
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\komentar_posts  $komentar_posts
     * @return \Illuminate\Http\Response
     */
    public function show(komentar_posts $komentar_posts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\komentar_posts  $komentar_posts
     * @return \Illuminate\Http\Response
     */
    public function edit(komentar_posts $komentar_posts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\komentar_posts  $komentar_posts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, komentar_posts $komentar_posts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\komentar_posts  $komentar_posts
     * @return \Illuminate\Http\Response
     */
    public function destroy(komentar_posts $komentar_posts)
    {
        //
    }
}
