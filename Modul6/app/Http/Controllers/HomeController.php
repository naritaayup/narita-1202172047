<?php

namespace App\Http\Controllers;

use App\komentar_posts;
use App\post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = post::with('user', 'komentar_posts.user')->get();
        return view('home')->with('posts', $posts);
    }

    public function showProfile()
    {
        $posts = post::where('user_id', Auth::user()->id)->get();
        return view('profile')->with('posts', $posts);
    }

    public function editProfile()
    {
        return view('editprofile');
    }

    public function updateProfile(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->title = $request->input('title');
        $user->description = $request->input('desc');
        $user->url = $request->input('url');
        $filepath = 'images';
        $foto = $request->file('foto');
        $fotos = $foto->getClientOriginalName();
        $foto->move($filepath, $fotos);
        $user->avatar = $fotos;
        $user->save();

        return redirect('/profile');
    }
}
