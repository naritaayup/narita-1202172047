<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('newpost');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new post();
        $post->user_id = Auth::user()->id;
        $post->caption = $request->input('caption');
        $filepath = 'images';
        $foto = $request->file('image');
        $fotos = $foto->getClientOriginalName();
        $foto->move($filepath, $fotos);
        $post->image = $fotos;
        $post->save();
        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = post::with('user', 'komentar_posts')->where('id', $id)->first();
        return view('detailPost')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(post $post)
    {
        //
    }
    public function likes($id){
        post::find($id)->increment('likes');
        return redirect('home');
    }
}
