@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @foreach ($posts as $post)

                    <div class="card">
                        <div class="card-header">
                            <img src="images/{{$post->user->avatar}}" width="30px" height="30px"
                                 style="border-radius: 50%;">
                            {{$post->user->name}}
                        </div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <center>
                                <a href="/detailPost/{{$post->id}}">
                                    <img src="images/{{$post->image}}" width="300px" height="250px">
                                </a>
                            </center>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('postlike',['id'=>$post->id]) }} "><i class="fa fa-heart-o"></i></a>
                            <a href="#"><i class="fa fa-comment-o"></i></a><br>
                            {{$post->likes}} likes<br>
                            {{$post->user->email}}
                            {{$post->caption}}<br><br>

                            @foreach($post->komentar_posts as $comment)
                                {{$comment->user->email}} {{$comment->comment}}<br>
                            @endforeach
                        </div>
                        <form action="/postComment/{{$post->id}}" method="post">
                            @csrf
                            <div class="input-group">
                                <input type="text" class="form-control" name="comment" required
                                       placeholder="Add a comment..">
                                <button type="submit" class="btn btn-primary">Post</button>
                            </div>
                        </form>
                    </div>
                    <br>
                @endforeach

            </div>
        </div>
    </div>
@endsection