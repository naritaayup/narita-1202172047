@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Add New Post</h1>
    <p>Post Caption</p>
    <form method="POST" action="/postdata" enctype="multipart/form-data">
    @csrf
        <input type="text" name="caption" placeholder="Insert a Caption">
        <p> Post Image </p>
        <input type="file" name="image" placeholder="image">
        <input type="submit" value="Add new post">
    </form>
</div>
@endsection