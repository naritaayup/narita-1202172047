@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-5">
            <div class="col-md-4 text-center">
                <img style="width: 60%; height: 80%; border-radius: 50%" src="images/{{ \Illuminate\Support\Facades\Auth::user()->avatar }}">
            </div>
            <div class="col-md-6">
                <h4>{{\Illuminate\Support\Facades\Auth::user()->name}}</h4><br>
                <p><a href="/editprofile">Edit Profile</a><br>
                    <b>{{$posts->count()}}</b> Post</p>
                <p>
                    <b>{{\Illuminate\Support\Facades\Auth::user()->title}}</b><br>{{\Illuminate\Support\Facades\Auth::user()->description}}
                    <br><a href="#">{{\Illuminate\Support\Facades\Auth::user()->url}}</a>
                </p>
            </div>
            <div class="col-md-2 text-right">
                <a href="/addPost">Add New Post</a>
            </div>
        </div>
        <div class="row justify-content-left my-1">
            @foreach($posts as $post)
                <div class="col-md-4 my-3">
                    <a href="#"><img src="images/{{$post->image}}" style="width:100%; height: 60%"></a>
                </div>
            @endforeach
        </div>

    </div>

@endsection