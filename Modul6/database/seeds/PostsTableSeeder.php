<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'Selamat Datang',
            'image' => 'Hallo.jpg',
            'likes' => '40',

        ]);
        
        DB::table('posts')->insert([
            'user_id' => '2',
            'caption' => 'Assalamualaikum',
            'image' => 'Miyong.jpg',
            'likes' => '20',

        ]);

        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'Pagi Pagi pasti Happy',
            'image' => 'White.jpg',
            'likes' => '30',

        ]);

        DB::table('posts')->insert([
            'user_id' => '2',
            'caption' => 'Assalamualaikum',
            'image' => 'EAD.png',
            'likes' => '50',

        ]);
    }
}
