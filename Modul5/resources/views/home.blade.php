@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @foreach ($posts as $post)

            <div class="card">
                <div class="card-header">
                    <img src="{{$post->avatar}}" width="30px" height="30px" style="border-radius: 50%;">
                    {{$post->name}}
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <center>
                        <img src="{{$post->image}}" width="300px" height="250px">
                    </center>
                </div>
                <div class="card-footer">
                    {{$post->email}} <br>
                    {{$post->caption}}</div>

            </div>
            <br>
            @endforeach

        </div>
    </div>
</div>
@endsection