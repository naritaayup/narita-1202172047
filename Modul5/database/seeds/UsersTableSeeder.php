<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Narita',
            'email' => 'Naritaayup'.'@gmail.com',
            'password' => bcrypt('password'),
            'avatar' => 'Miyong.jpg',

        ]);

        DB::table('users')->insert([
            'name' => 'Retna',
            'email' => 'Retna'.'@gmail.com',
            'password' => bcrypt('password'),
            'avatar' => 'Flamingo.jpg',
        ]);
    }
}